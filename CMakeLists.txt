find_package(PkgConfig)

cmake_minimum_required(VERSION 3.6)
project(merklehashtree)

set(CMAKE_CXX_STANDARD 14)
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror")

find_package(Boost)
IF (Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIR})
endif()
set (Boost_USE_STATIC_LIBS OFF) # enable dynamic linking
set (Boost_USE_MULTITHREAD ON)  # enable multithreading
find_package(Boost COMPONENTS REQUIRED serialization system unit_test_framework)

find_package(OpenSSL)

find_package(websocketpp)

pkg_check_modules(RelayMQ REQUIRED librelaymq)


set(SOURCE_FILES src/enities/Data.cpp src/enities/Data.h src/enities/DataSerialization.h src/hashing/HashAlgorithm.h src/hashing/Sha256.cpp src/hashing/Sha256.h src/binarysearchtree/Tree.h src/binarysearchtree/MerkleHashTree.cpp src/binarysearchtree/MerkleHashTree.h src/types/ByteVector.h src/types/HashValue.h src/merklehashtreeverifier/Verifier.h src/merklehashtreeverifier/MerkleHashVerifier.cpp src/merklehashtreeverifier/MerkleHashVerifier.h src/binarysearchtree/MerkleHashVerificationTree.h src/binarysearchtree/MerkleHashVerificationTreeSerialization.h src/helper/Unused.h src/binarysearchtree/MerkleHashTreeSerialization.h src/api/Api.h src/api/WebsocketApi.cpp src/api/WebsocketApi.h src/binarysearchtree/Tree.cpp src/helper/DisallowCopy.h src/subjects/VerifierSubject.h src/subjects/StorerSubject.h src/subjects/Message.h src/subjects/ApiSubject.h)
set(MAIN_SOURCE_FILE src/main.cpp)
set(TEST_SOURCE_FILE src/testMain.cpp)

add_executable(merklehashtree ${SOURCE_FILES} ${MAIN_SOURCE_FILE})
add_executable(tests ${SOURCE_FILES} ${TEST_SOURCE_FILE})

target_link_libraries (merklehashtree ${Boost_LIBRARIES})
target_link_libraries(tests ${Boost_LIBRARIES})

target_link_libraries(merklehashtree ${RelayMQ_LIBRARIES})
target_link_libraries(tests ${RelayMQ_LIBRARIES})
target_include_directories(merklehashtree PUBLIC ${RelayMQ_INCLUDE_DIRS})
target_include_directories(tests PUBLIC ${RelayMQ_INCLUDE_DIRS})
target_compile_options(merklehashtree PUBLIC ${RelayMQ_CFLAGS_OTHER})
target_compile_options(tests PUBLIC ${RelayMQ_CFLAGS_OTHER})

target_link_libraries (merklehashtree ${OPENSSL_LIBRARIES})
target_link_libraries(tests ${OPENSSL_LIBRARIES})

target_include_directories(merklehashtree PUBLIC ${websocketpp_INCLUDE_DIR})
target_include_directories(tests PUBLIC ${websocketpp_INCLUDE_DIR})

target_link_libraries(merklehashtree relaymqcpp)
target_link_libraries(tests relaymqcpp)

target_link_libraries(merklehashtree pthread)
target_link_libraries(tests pthread)
