#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE ApplicationTests

#include <boost/test/unit_test.hpp>

#include "binarysearchtree/MerkleHashTreeSerializationTest.cpp"
#include "binarysearchtree/MerkleHashVerificationTreeSerializationTest.cpp"
#include "binarysearchtree/TreeTest.cpp"

#include "hashing/Sha256Test.cpp"

#include "merklehashtreeverifier/MerkleHashVerifierTest.cpp"

#include "subjects/MessageSerializationTest.cpp"
