#include <iostream>
#include <ApplicationContext.h>
#include "binarysearchtree/Tree.h"
#include "api/WebsocketApi.h"
#include "subjects/VerifierSubject.h"
#include "subjects/ApiSubject.h"
#include "subjects/StorerSubject.h"

int main() {
    ApplicationContext::Init();

    auto verifier = ApplicationContext::Create<VerifierSubject>();
    auto storer = ApplicationContext::Create<StorerSubject>();
    auto api = ApplicationContext::Create<ApiSubject>();

    Msg verifyIntroduce;
    verifyIntroduce.type = IntroduceVerifier;
    api->Send(verifyIntroduce.toString(), {verifier});

    Msg storerIntroduce;
    storerIntroduce.type = IntroduceStorer;
    const std::string &storerIntroduceMsg = storerIntroduce.toString();
    api->Send(storerIntroduceMsg, {storer});
    verifier->Send(storerIntroduceMsg, {storer});

    Msg apiIntroduce;
    apiIntroduce.type = IntroduceApi;
    storer->Send(apiIntroduce.toString(), {api});

    ApplicationContext::Start();

    return 0;
}