#pragma once

#include <websocketpp/config/asio_no_tls.hpp>

#include <websocketpp/server.hpp>

#include <iostream>
#include <set>

#include <websocketpp/common/thread.hpp>
#include <queue>
#include "../binarysearchtree/Tree.h"
#include "../binarysearchtree/MerkleHashTreeSerialization.h"
#include "Api.h"
#include "../helper/Unused.h"
#include "../helper/DisallowCopy.h"
#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/hex.hpp>

typedef websocketpp::server<websocketpp::config::asio> server;

using websocketpp::connection_hdl;
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

using websocketpp::lib::thread;
using websocketpp::lib::mutex;
using websocketpp::lib::lock_guard;
using websocketpp::lib::unique_lock;
using websocketpp::lib::condition_variable;

class WebsocketApi : public Api {
public:
    WebsocketApi() {
        mServer.clear_access_channels(websocketpp::log::alevel::all);
        mServer.init_asio();

        mServer.set_open_handler(bind(&WebsocketApi::on_open, this, ::_1));
        mServer.set_close_handler(bind(&WebsocketApi::on_close, this, ::_1));
        mServer.set_message_handler(bind(&WebsocketApi::on_message, this, ::_1, ::_2));
    }

    void start() {
        mServer.set_reuse_addr(true);
        mServer.listen(5555);
        mServer.start_accept();

        mApiThread = std::thread([&]() {
            try {
                mServer.run();
            } catch (const std::exception &e) {
                std::cout << e.what() << std::endl;
            }
        });
    }

    DISALLOW_COPY_AND_ASSIGN(WebsocketApi)

    void on_open(connection_hdl hdl) {
        {
            lock_guard<mutex> guard(mConnectionLock);
            mConnections.insert(hdl);
        }
        requires_new_notify();
    }

    void on_close(connection_hdl hdl) {
        lock_guard<mutex> guard(mConnectionLock);
        mConnections.erase(hdl);
    }

    void on_message(connection_hdl hdl, server::message_ptr msg) {
        UNUSED(hdl);

        boost::property_tree::ptree message;
        const std::string &payload = msg->get_payload();
        std::istringstream is (payload);
        boost::property_tree::read_json (is, message);
        const std::string &type = message.get<std::string>("type");

        if (type == "deleteNode") {
            const std::string &hexHash = message.get<std::string>("nodeId");
            HashValue hash;
            boost::algorithm::unhex(hexHash, back_inserter(hash));
            delete_notify(hash);
        } else if (type == "insertNode") {
            ByteVector data;
            for (auto &item: message.get_child("data")) {
                unsigned char byte = item.second.get_value<unsigned char>();
                data.push_back(byte);
            }
            insert_notify(data);
        } else {
            std::cerr << "unknown message type: " << type << std::endl;
            std::cerr << "message was: " << payload << std::endl;
        }
    }

    virtual void notifyClientAboutUpdatedTree(const Tree &tree) override {
        lock_guard<mutex> guard(mConnectionLock);

        auto ptree = toPropertyTree(tree);

        boost::property_tree::ptree message;
        message.put("type", "Tree");
        message.add_child("tree", ptree);

        std::ostringstream oss;
        boost::property_tree::write_json(oss, message);
        std::string jsonMessage = oss.str();

        for (auto &connection: mConnections) {
            mServer.send(connection, jsonMessage, websocketpp::frame::opcode::text);
        }
    }

private:
    typedef std::set<connection_hdl, std::owner_less<connection_hdl> > con_list;

    server mServer;
    con_list mConnections;

    mutex mConnectionLock;

    std::thread mApiThread;
};
