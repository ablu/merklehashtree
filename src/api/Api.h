#pragma once

#include "../binarysearchtree/Tree.h"

#include <boost/signals2/signal.hpp>

class Api {
public:
    virtual void notifyClientAboutUpdatedTree(const Tree &tree) = 0;

    boost::signals2::signal<void()> requires_new_notify;
    boost::signals2::signal<void(const HashValue &)> delete_notify;
    boost::signals2::signal<void(const ByteVector &)> insert_notify;
};
