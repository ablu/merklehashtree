#pragma once


#include "Verifier.h"
#include "../binarysearchtree/MerkleHashTree.h"
#include "../hashing/Sha256.h"

class MerkleHashVerifier : public Verifier {
public:
    virtual bool acceptInsert(const HashValue &dataToInsert,
                              std::unique_ptr<MerkleHashVerificationTree> &verificationTree,
                              const HashValue &verificationHashAfterInsert) override;

private:
    HashValue calculateDigest(const MerkleHashVerificationTree &node);

    HashValue mDigest;
    Sha256 mHash;
};

