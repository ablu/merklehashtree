#pragma once

#include "../types/HashValue.h"
#include "../binarysearchtree/MerkleHashTree.h"
#include "../binarysearchtree/MerkleHashVerificationTree.h"

class Verifier {
public:
    virtual bool acceptInsert(const HashValue &dataToInsert,
                              std::unique_ptr<MerkleHashVerificationTree> &verificationTree,
                              const HashValue &verificationHashAfterInsert) = 0;
};