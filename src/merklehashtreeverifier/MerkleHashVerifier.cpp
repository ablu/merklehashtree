#include <stack>
#include <boost/algorithm/hex.hpp>
#include <iostream>
#include "MerkleHashVerifier.h"
#include "../binarysearchtree/MerkleHashTree.h"

bool isLeaf(const MerkleHashVerificationTree &node) {
    return !node.left && !node.right;
}

inline void print_tree(const MerkleHashVerificationTree &tree) {
    std::string hash;
    boost::algorithm::hex(tree.hashValue, back_inserter(hash));
    std::cout << "{hash: " << hash;
    std::string dataHash;
    boost::algorithm::hex(tree.dataHashValue, back_inserter(dataHash));
    std::cout << ", dataHash: " << dataHash;

    if (tree.left) {
        std::cout << ", left child: ";
        print_tree(*tree.left);
    }
    if (tree.right) {
        std::cout << ", right child: ";
        print_tree(*tree.right);
    }

    std::cout << "}";
}

bool MerkleHashVerifier::acceptInsert(const HashValue &dataToInsert,
                                      std::unique_ptr<MerkleHashVerificationTree> &verificationTree,
                                      const HashValue &claimedDigestAfterInsert) {
    if (!verificationTree) {
        if (claimedDigestAfterInsert != dataToInsert) {
            return false;
        }
    } else {
        const HashValue &digestBeforeInsert = verificationTree->hashValue;
        if (mDigest != digestBeforeInsert) {
            return false;
        }

        MerkleHashVerificationTree treeAfterInsert = *verificationTree;
        treeAfterInsert.insert(dataToInsert);
        std::cout << "Tree after insert: ";
        print_tree(treeAfterInsert);
        std::cout << std::endl;
        const HashValue &digestAfterInsert = treeAfterInsert.hashValue;
        if (digestAfterInsert != claimedDigestAfterInsert) {
            return false;
        }
    }
    mDigest = claimedDigestAfterInsert;
    return true;
}
