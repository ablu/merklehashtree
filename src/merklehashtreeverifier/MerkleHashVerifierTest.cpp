#include <boost/test/unit_test.hpp>
#include "../binarysearchtree/Tree.h"
#include "MerkleHashVerifier.h"

inline void print_tree(const Tree &tree) {
    std::string hash;
    boost::algorithm::hex(tree.hash(), back_inserter(hash));
    std::cout << "{hash: " << hash;
    std::string dataHash;
    boost::algorithm::hex(tree.dataHash().get(), back_inserter(dataHash));
    std::cout << ", dataHash: " << dataHash;

    if (tree.left()) {
        std::cout << ", left child: ";
        print_tree(*tree.left());
    }
    if (tree.right()) {
        std::cout << ", right child: ";
        print_tree(*tree.right());
    }

    std::cout << "}";
}

inline void print_tree(const MerkleHashVerificationTree &tree) {
    std::string hash;
    boost::algorithm::hex(tree.hashValue, back_inserter(hash));
    std::cout << "{hash: " << hash;
    std::string dataHash;
    boost::algorithm::hex(tree.dataHashValue, back_inserter(dataHash));
    std::cout << ", dataHash: " << dataHash;

    if (tree.left) {
        std::cout << ", left child: ";
        print_tree(*tree.left);
    }
    if (tree.right) {
        std::cout << ", right child: ";
        print_tree(*tree.right);
    }

    std::cout << "}";
}

BOOST_AUTO_TEST_CASE(verifierTest) {
    Tree tree;
    Sha256 hash;

    MerkleHashVerifier verifier;

    auto data1 = std::make_unique<Data>();
    HashValue hash1 = hash.hash({1});
    data1->hash = hash1;
    auto verificationTree1 = tree.insert(std::move(data1));
    print_tree(tree);
    std::cout << std::endl << "verification: null" << std::endl;
    BOOST_CHECK(verifier.acceptInsert(hash1, verificationTree1, tree.hash()));
    //HashValue invalidHash = {9};
    //BOOST_CHECK(!verifier.acceptInsert(invalidHash, *verificationTree1, tree.hash()));

    auto data2 = std::make_unique<Data>();
    HashValue hash2 = hash.hash({2});
    data2->hash = hash2;
    auto verificationTree2 = tree.insert(std::move(data2));
    print_tree(tree);
    std::cout << std::endl << "verification: ";
    print_tree(*verificationTree2);
    std::cout << std::endl;
    BOOST_CHECK(verifier.acceptInsert(hash2, verificationTree2, tree.hash()));

    auto data3 = std::make_unique<Data>();
    HashValue hash3 = hash.hash({3});
    data3->hash = hash3;
    auto verificationTree3 = tree.insert(std::move(data3));
    print_tree(tree);
    std::cout << std::endl << "verification: ";
    print_tree(*verificationTree3);
    std::cout << std::endl;
    BOOST_CHECK(verifier.acceptInsert(hash3, verificationTree3, tree.hash()));

    auto data4 = std::make_unique<Data>();
    HashValue hash4 = hash.hash({4});
    data4->hash = hash4;
    auto verificationTree4 = tree.insert(std::move(data4));
    print_tree(tree);
    std::cout << std::endl << "verification: ";
    print_tree(*verificationTree4);
    std::cout << std::endl;
    BOOST_CHECK(verifier.acceptInsert(hash4, verificationTree4, tree.hash()));
}
