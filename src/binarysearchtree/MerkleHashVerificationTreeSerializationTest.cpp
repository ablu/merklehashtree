#include <boost/test/unit_test.hpp>

#include <boost/archive/text_oarchive.hpp>
#include <sstream>
#include "MerkleHashVerificationTree.h"
#include "MerkleHashVerificationTreeSerialization.h"


BOOST_AUTO_TEST_CASE(verificationSerializationTest) {
    std::stringstream text;
    boost::archive::text_oarchive oa(text);

    MerkleHashVerificationTree tree;
    tree.dataHashValue = {0};
    tree.hashValue = {1};

    tree.left = std::make_unique<MerkleHashVerificationTree>();
    tree.left->hashValue = {2};

    oa << tree;

    boost::archive::text_iarchive ia(text);
    MerkleHashVerificationTree deserializedTree;
    ia >> deserializedTree;
    BOOST_CHECK_EQUAL_COLLECTIONS(deserializedTree.dataHashValue.begin(), deserializedTree.dataHashValue.end(),
                                  tree.dataHashValue.begin(), tree.dataHashValue.end());
    BOOST_CHECK_EQUAL_COLLECTIONS(deserializedTree.hashValue.begin(), deserializedTree.hashValue.end(),
                                  tree.hashValue.begin(), tree.hashValue.end());
    BOOST_CHECK(tree.left);
    BOOST_CHECK_EQUAL_COLLECTIONS(deserializedTree.left->hashValue.begin(), deserializedTree.left->hashValue.end(),
                                  tree.left->hashValue.begin(), tree.left->hashValue.end());
}
