#include <boost/test/unit_test.hpp>
#include "Tree.h"

BOOST_AUTO_TEST_CASE(emptyTreeTest) {
    Tree tree;
    BOOST_CHECK(!tree.left());
    BOOST_CHECK(!tree.right());
}

BOOST_AUTO_TEST_CASE(emptyTreeInsertTest) {
    Tree tree;
    auto data = std::make_unique<Data>();
    HashValue hash = {1};
    data->hash = hash;
    tree.insert(std::move(data));

    BOOST_CHECK_EQUAL_COLLECTIONS(tree.dataHash().get().begin(), tree.dataHash().get().end(),
                                  hash.begin(), hash.end());
    BOOST_CHECK(!tree.left());
    BOOST_CHECK(!tree.right());
}

BOOST_AUTO_TEST_CASE(treeInsertTest) {
    auto initialData = std::make_unique<Data>();
    auto leftData = std::make_unique<Data>();
    auto rightData = std::make_unique<Data>();
    HashValue initialHash = {5};
    HashValue leftHash = {1};
    HashValue rightHash = {9};
    initialData->hash = initialHash;
    leftData->hash = leftHash;
    rightData->hash = rightHash;

    Tree tree(std::move(initialData));

    auto verificationTree = tree.insert(std::move(leftData));
    BOOST_CHECK_EQUAL_COLLECTIONS(tree.left()->dataHash().get().begin(), tree.left()->dataHash().get().end(),
                                  leftHash.begin(), leftHash.end());
    BOOST_CHECK(!verificationTree->left);
    BOOST_CHECK(!verificationTree->right);

    verificationTree = tree.insert(std::move(rightData));
    BOOST_CHECK_EQUAL_COLLECTIONS(tree.right()->dataHash().get().begin(), tree.right()->dataHash().get().end(),
                                  rightHash.begin(), rightHash.end());
    BOOST_CHECK(verificationTree->left);
    BOOST_CHECK(!verificationTree->right);
}
