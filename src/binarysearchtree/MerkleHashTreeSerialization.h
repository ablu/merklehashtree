#pragma once

#include <boost/property_tree/ptree.hpp>
#include "Tree.h"
#include "../helper/Unused.h"

using boost::property_tree::ptree;

static ptree toPropertyTree(const HashValue &value) {
    ptree propertyTree;
    for (auto &byte: value) {
        ptree valueTree;
        valueTree.put("", byte);
        propertyTree.push_back(std::make_pair("", valueTree));
    }
    return propertyTree;
}

static ptree toPropertyTree(const Tree &tree) {
    ptree propertyTree;

    if (!tree.data()) {
        return propertyTree;
    }

    if (tree.left()) {
        propertyTree.add_child("left", toPropertyTree(*tree.left()));
    }
    if (tree.right()) {
        propertyTree.add_child("right", toPropertyTree(*tree.right()));
    }
    propertyTree.add_child("hash", toPropertyTree(tree.hash()));
    propertyTree.add_child("dataHash", toPropertyTree(tree.dataHash().get()));
    propertyTree.add_child("data", toPropertyTree(tree.data().get().data));

    return propertyTree;
}
