#pragma once

#include <memory>
#include "../types/HashValue.h"
#include "../hashing/Sha256.h"

class MerkleHashVerificationTree {
public:
    MerkleHashVerificationTree()
    {}

    MerkleHashVerificationTree(const MerkleHashVerificationTree &orig)
    {
        hashValue = orig.hashValue;
        dataHashValue = orig.dataHashValue;
        if (orig.left) {
            left = std::unique_ptr<MerkleHashVerificationTree>(new MerkleHashVerificationTree(*orig.left));
        }
        if (orig.right) {
            right = std::unique_ptr<MerkleHashVerificationTree>(new MerkleHashVerificationTree(*orig.right));
        }
    }

    void insert(const HashValue &value) {
        if (value < dataHashValue) {
            if (left) {
                left->insert(value);
            } else {
                auto newNode = std::make_unique<MerkleHashVerificationTree>();
                newNode->dataHashValue = value;
                newNode->hashValue = value;
                left = std::move(newNode);
            }
        } else {
            if (right) {
                right->insert(value);
            } else {
                auto newNode = std::make_unique<MerkleHashVerificationTree>();
                newNode->dataHashValue = value;
                newNode->hashValue = value;
                right = std::move(newNode);
            }
        }
        recalculateHash();
    }

    std::unique_ptr<MerkleHashVerificationTree> left;
    std::unique_ptr<MerkleHashVerificationTree> right;
    HashValue hashValue;
    HashValue dataHashValue;

    Sha256 mHashAlgorithm;

private:
    void recalculateHash() {
        auto combinedHashes = HashValue();
        if (left) {
            combinedHashes.insert(combinedHashes.end(), left->hashValue.begin(), left->hashValue.end());
        }
        combinedHashes.insert(combinedHashes.end(), dataHashValue.begin(), dataHashValue.end());
        if (right) {
            combinedHashes.insert(combinedHashes.end(), right->hashValue.begin(), right->hashValue.end());
        }
        hashValue = mHashAlgorithm.hash(combinedHashes);
    }
};