#include <boost/test/unit_test.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "Tree.h"
#include "MerkleHashTreeSerialization.h"

#include <sstream>
#include <iostream>

BOOST_AUTO_TEST_CASE(jsonTreeSerializationTest) {
    auto initialData = std::make_unique<Data>();
    auto leftData = std::make_unique<Data>();
    auto rightData = std::make_unique<Data>();
    HashValue initialHash = {5};
    HashValue leftHash = {1};
    HashValue rightHash = {9};
    initialData->hash = initialHash;
    leftData->hash = leftHash;
    rightData->hash = rightHash;
    Tree tree(std::move(initialData));
    tree.insert(std::move(leftData));
    tree.insert(std::move(rightData));

    auto ptree = toPropertyTree(tree);

    std::ostringstream oss;
    boost::property_tree::write_json(oss, ptree);
}