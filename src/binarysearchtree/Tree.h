#pragma once

#include <memory>
#include <assert.h>
#include "../enities/Data.h"
#include "../hashing/HashAlgorithm.h"
#include "../hashing/Sha256.h"
#include "MerkleHashVerificationTree.h"
#include "../helper/Unused.h"

#include <boost/optional.hpp>
#include <boost/signals2/signal.hpp>
#include <boost/serialization/access.hpp>

class Tree {
public:
    Tree(const Tree &other) {
        mOwnHash = other.mOwnHash;
        if (other.mLeft) {
            mLeft = std::make_unique<Tree>(*other.mLeft);
        }
        if (other.mRight) {
            mRight = std::make_unique<Tree>(*other.mRight);
        }
        if (other.mData) {
            mData = std::make_unique<Data>(*other.mData);
        }
    }

    Tree() {
        mHashAlgorithm = std::unique_ptr<HashAlgorithm>(new Sha256());
    }

    Tree(std::unique_ptr<Data> dataHash)
            : Tree()
    {
        mData = std::move(dataHash);
    }

    std::unique_ptr<MerkleHashVerificationTree> insert(std::unique_ptr<Data> newData);

    std::unique_ptr<MerkleHashVerificationTree> deleteData(const HashValue &hash);

    const std::unique_ptr<Tree> &left() const { return mLeft; }

    const std::unique_ptr<Tree> &right() const { return mRight; }
    const HashValue &hash() const { return mOwnHash; }

    const boost::optional<HashValue &> dataHash() const {
        if (mData) {
            return mData->hash;
        } else {
            return boost::none;
        }
    }

    const boost::optional<Data &> data() const {
        if (mData) {
            return *mData;
        } else {
            return boost::none;
        }
    }

    boost::signals2::signal<void(Tree &)> on_tree_update;

private:
    void recalculateHash();

    std::unique_ptr<Tree> mLeft;
    std::unique_ptr<Tree> mRight;

    HashValue mOwnHash;

    std::unique_ptr<Data> mData;

    std::unique_ptr<HashAlgorithm> mHashAlgorithm;

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        UNUSED(version);

        ar & mLeft;
        ar & mRight;
        ar & mOwnHash;
        ar & mData;
    }

    std::_MakeUniq<MerkleHashVerificationTree>::__single_object
    getSurroundingsForDeletion(const std::unique_ptr<Tree> &node) const;
};
