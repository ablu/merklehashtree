#pragma once

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include "MerkleHashVerificationTree.h"
#include <boost/serialization/vector.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include "../helper/Unused.h"

namespace boost{
    namespace serialization {
        template<class Archive>
        void serialize(Archive &ar, MerkleHashVerificationTree &tree, const unsigned int version)
        {
            UNUSED(version);

            ar & tree.left;
            ar & tree.right;
            ar & tree.hashValue;
            ar & tree.dataHashValue;
        }
    }
}
