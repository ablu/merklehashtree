#include "Tree.h"

std::unique_ptr<MerkleHashVerificationTree> Tree::insert(std::unique_ptr<Data> newData) {
    if (!mData) {
        mData = std::move(newData);
        recalculateHash();
        on_tree_update(*this);
        return nullptr;
    }
    auto verificationTree = std::make_unique<MerkleHashVerificationTree>();
    verificationTree->hashValue = mOwnHash;
    verificationTree->dataHashValue = mData->hash;

    if (*newData < *mData) {
        if (mLeft) {
            auto subVerificationTree = mLeft->insert(std::move(newData));
            verificationTree->left = std::move(subVerificationTree);
        } else {
            mLeft = std::make_unique<Tree>(std::move(newData));
            mLeft->recalculateHash();
        }
        if (mRight) {
            verificationTree->right = std::make_unique<MerkleHashVerificationTree>();
            verificationTree->right->hashValue = mRight->hash();
        }
    } else {
        if (mRight) {
            auto subVerificationTree = mRight->insert(std::move(newData));
            verificationTree->right = std::move(subVerificationTree);
        } else {
            mRight = std::make_unique<Tree>(std::move(newData));
            mRight->recalculateHash();
        }
        if (mLeft) {
            verificationTree->left = std::make_unique<MerkleHashVerificationTree>();
            verificationTree->left->hashValue = mLeft->hash();
        }
    }

    recalculateHash();
    on_tree_update(*this);
    return verificationTree;
}

void Tree::recalculateHash() {
    if (!mLeft && !mRight) {
        mOwnHash = mData->hash;
        return;
    }

    auto combinedHashes = HashValue();
    if (mLeft) {
        combinedHashes.insert(combinedHashes.end(), mLeft->mOwnHash.begin(), mLeft->mOwnHash.end());
    }
    combinedHashes.insert(combinedHashes.end(), mData->hash.begin(), mData->hash.end());
    if (mRight) {
        combinedHashes.insert(combinedHashes.end(), mRight->mOwnHash.begin(), mRight->mOwnHash.end());
    }
    mOwnHash = mHashAlgorithm->hash(combinedHashes);
}

std::unique_ptr<MerkleHashVerificationTree> Tree::deleteData(const HashValue &hash) {
    assert(mData);

    auto verificationTree = std::make_unique<MerkleHashVerificationTree>();
    verificationTree->hashValue = mOwnHash;
    verificationTree->dataHashValue = mData->hash;

    if (mData->hash == hash) {
        mData = nullptr;
        mOwnHash = HashValue();
        on_tree_update(*this);
        return verificationTree;
    }

    if (hash < mData->hash) {
        assert(mLeft);
        if (mLeft->dataHash().get() == hash) {
            std::unique_ptr<MerkleHashVerificationTree> subVerificationTree = getSurroundingsForDeletion(mLeft);
            verificationTree->left = std::move(subVerificationTree);
        } else {
            std::unique_ptr<MerkleHashVerificationTree> subVerificationTree = mLeft->deleteData(hash);
            verificationTree->left = std::move(subVerificationTree);
        }
    }

    return verificationTree;
}

std::unique_ptr<MerkleHashVerificationTree> Tree::getSurroundingsForDeletion(const std::unique_ptr<Tree> &node) const {
    auto subVerificationTree = std::make_unique<MerkleHashVerificationTree>();
    subVerificationTree->hashValue = node->mOwnHash;
    subVerificationTree->dataHashValue = node->mData->hash;

    if (node->mLeft) {
        auto subVerificationTreeLeft = std::make_unique<MerkleHashVerificationTree>();
        subVerificationTreeLeft->hashValue = node->mLeft->mOwnHash;
        subVerificationTreeLeft->dataHashValue = node->mLeft->mData->hash;
        subVerificationTree->left = move(subVerificationTreeLeft);
    }
    if (node->mRight) {
        auto subVerificationTreeRight = std::make_unique<MerkleHashVerificationTree>();
        subVerificationTreeRight->hashValue = node->mRight->mOwnHash;
        subVerificationTreeRight->dataHashValue = node->mRight->mData->hash;
        subVerificationTree->right = move(subVerificationTreeRight);
    }
    return subVerificationTree;
}
