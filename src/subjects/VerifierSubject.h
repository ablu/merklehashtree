#pragma once

#include <Relay.h>
#include <Subject.h>
#include "Message.h"
#include "../merklehashtreeverifier/MerkleHashVerifier.h"

class VerifierSubject: public Subject{
public:
    VerifierSubject() {
    }
private:
    RelayRef mStorer;
    MerkleHashVerifier mVerifier;
    boost::optional<HashValue> mCurrentHashToConfirm;
    Sha256 mHashAlgorithm;

    void onTimeout() override {
    }

    virtual void onMessage(RelayRef receiver, std::string msg, std::vector<RelayRef> refs) override {
        std::cout << "on verifier message" << std::endl;
        Msg message = Msg::fromString(msg);
        if (message.type == IntroduceStorer){
            mStorer = refs[0];
        } else if (message.type == Store) {
            if (mCurrentHashToConfirm) {
                std::cerr << "Still waiting for an insert confirmation! Refusing message" << std::endl;
                return;
            }
            mCurrentHashToConfirm = mHashAlgorithm.hash(message.data.get());
            // forward
            assert(mStorer->Send(msg, {receiver}));
            std::cout << "forwarded store" << std::endl;
        } else if (message.type == ConfirmStore) {
            std::cout << "confirming store..." << std::endl;
            auto &verificationHash = message.verificationHash.get();
            auto &verificationTree = message.verificationTree;
            bool accept = mVerifier.acceptInsert(mCurrentHashToConfirm.get(), verificationTree, verificationHash);
            if (!accept) {
                std::cerr << "Failed to confirm insert!" << std::endl;
                assert(false);
            }
            mCurrentHashToConfirm = boost::none;
        } else {
            std::cerr << "Received unknown message: " << message.type << std::endl;
        }
    }
};
