#pragma once

#include <string>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/optional.hpp>
#include <boost/optional.hpp>
#include "../types/HashValue.h"
#include "../binarysearchtree/MerkleHashVerificationTree.h"
#include "../helper/Unused.h"
#include "../binarysearchtree/MerkleHashVerificationTreeSerialization.h"
#include "../enities/DataSerialization.h"

enum MessageType {
    Init = 0,
    IntroduceVerifier,
    IntroduceStorer,
    IntroduceApi,
    Store,
    ConfirmStore,
    RequestUpdate,
    TreeUpdate,
    Invalid
};

class Msg {
public:
    MessageType type = Invalid;
    boost::optional<HashValue> hash;
    boost::optional<HashValue> verificationHash;
    boost::optional<ByteVector> data;
    std::unique_ptr<MerkleHashVerificationTree> verificationTree;
    std::unique_ptr<Tree> tree = nullptr;

    static Msg fromString(const std::string &message) {
        std::istringstream text(message);
        boost::archive::text_iarchive ia(text);
        Msg msg;
        ia >> msg;
        return msg;
    }

    inline std::string toString() {
        assert(type != Invalid);
        std::ostringstream text;
        boost::archive::text_oarchive oa(text);
        oa << *this;
        return text.str();
    }
};

namespace boost{
    namespace serialization {
        template<class Archive>
        void serialize(Archive &ar, Msg &msg, const unsigned int version)
        {
            UNUSED(version);

            ar & msg.type;
            ar & msg.hash;
            ar & msg.verificationHash;
            ar & msg.data;
            ar & msg.verificationTree;
            ar & msg.tree;
        }
    }
}
