#pragma once

#include <Relay.h>
#include <Subject.h>
#include "Message.h"
#include "../binarysearchtree/Tree.h"

inline void print_tree(const Tree &tree) {
    std::string hash;
    boost::algorithm::hex(tree.hash(), back_inserter(hash));
    std::cout << "{hash: " << hash;
    std::string dataHash;
    boost::algorithm::hex(tree.dataHash().get(), back_inserter(dataHash));
    std::cout << ", dataHash: " << dataHash;

    if (tree.left()) {
        std::cout << ", left child: ";
        print_tree(*tree.left());
    }
    if (tree.right()) {
        std::cout << ", right child: ";
        print_tree(*tree.right());
    }

    std::cout << "}";
}

class StorerSubject: public Subject {
public:
    StorerSubject() {
    }

    const Tree &tree() const {
        return mTree;
    }

private:
    Tree mTree;
    Sha256 mHash;
    RelayRef mApi;

    void onTimeout() override {
    }

    virtual void onMessage(RelayRef receiver, std::string msg, std::vector<RelayRef> refs) override {
        UNUSED(receiver);

        Msg message = Msg::fromString(msg);
        if (message.type == Store) {
            std::cout << "store" << std::endl;
            auto data = std::make_unique<Data>();
            data->data = message.data.get();
            const HashValue &hash = mHash.hash(data->data);
            data->hash = hash;

            auto verificationTree = mTree.insert(std::move(data));
            std::cout << "Tree: ";
            print_tree(mTree);
            std::cout << std::endl;

            Msg msg;
            msg.type = ConfirmStore;
            msg.verificationTree = std::move(verificationTree);
            msg.hash = hash;
            msg.verificationHash = mTree.hash();

            assert(refs[0]->Send(msg.toString(), {}));
            std::cout << "sent confirmation" << std::endl;
        } else if (message.type == IntroduceApi) {
            mApi = refs[0];
            mTree.on_tree_update.connect([&](const Tree &tree) {
                Msg msg;
                msg.type = TreeUpdate;
                msg.tree = std::make_unique<Tree>(tree);
                assert(mApi->Send(msg.toString(), {}));
            });
        } else if (message.type == RequestUpdate) {
            Msg msg;
            msg.type = TreeUpdate;
            msg.tree = std::make_unique<Tree>(mTree);
            mApi->Send(msg.toString(), {});
        } else {
            std::cerr << "Received unknown message: " << message.type << std::endl;
        }
    }
};
