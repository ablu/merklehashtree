#pragma once

#include <Relay.h>
#include <Subject.h>
#include "Message.h"
#include "../api/WebsocketApi.h"

class ApiSubject: public Subject {
public:
    ApiSubject() {
    }

private:
    RelayRef mVerifier;
    RelayRef mStorer;

    WebsocketApi mApi;

    void onTimeout() override {
    }

    virtual void onMessage(RelayRef receiver, std::string msg, std::vector<RelayRef> refs) override {
        UNUSED(receiver);

        Msg message = Msg::fromString(msg);
        if (message.type == IntroduceVerifier){
            mVerifier = refs[0];
            mApi.insert_notify.connect([&](const ByteVector &data) {
                Msg msg;
                msg.type = Store;
                msg.data = data;
                assert(mVerifier->Send(msg.toString(), {}));
            });
            mApi.delete_notify.connect([&](const HashValue &hashValue) {
                UNUSED(hashValue);
            });
            mApi.start();
        } else if (message.type == IntroduceStorer){
            mStorer = refs[0];
            mApi.requires_new_notify.connect([&]() {
                Msg msg;
                msg.type = RequestUpdate;
                assert(mStorer->Send(msg.toString(), {}));
            });
        } else if (message.type == TreeUpdate) {
            mApi.notifyClientAboutUpdatedTree(*message.tree);
        } else {
            std::cerr << "Received unknown message: " << message.type << std::endl;
        }
    }
};
