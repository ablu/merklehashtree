#pragma once


#include "HashAlgorithm.h"

class Sha256 : public HashAlgorithm {
public:
    virtual HashValue hash(const ByteVector &data) const override;
};


