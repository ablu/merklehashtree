#pragma once


#include "../types/HashValue.h"
#include "../types/ByteVector.h"

class HashAlgorithm {
public:
    virtual HashValue hash(const ByteVector &data) const = 0;
};
