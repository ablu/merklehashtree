#include <boost/test/unit_test.hpp>
#include <boost/algorithm/hex.hpp>
#include <openssl/sha.h>
#include "Sha256.h"

BOOST_AUTO_TEST_CASE(sha256test) {
    Sha256 sha256;
    auto hash = sha256.hash(HashValue {'A'});

    const char *const expected_hash_hex = "559aead08264d5795d3909718cdd05abd49572e84fe55590eef31a88a08fdffd";
    HashValue expectedHash;
    boost::algorithm::unhex(expected_hash_hex, back_inserter(expectedHash));

    BOOST_CHECK_EQUAL_COLLECTIONS(hash.begin(), hash.end(),
                                  expectedHash.begin(), expectedHash.end());
}
