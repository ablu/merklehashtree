#include "Sha256.h"

#include <openssl/sha.h>

HashValue Sha256::hash(const ByteVector &data) const {
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, &data[0], data.size());
    SHA256_Final(hash, &sha256);

    return HashValue(hash, hash + sizeof hash / sizeof hash[0]);
}
