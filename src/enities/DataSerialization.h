#pragma once


#include "Data.h"
#include "../helper/Unused.h"

namespace boost{
    namespace serialization {
        template<class Archive>
        void serialize(Archive &ar, Data &data, const unsigned int version)
        {
            UNUSED(version);

            ar & data.data;
            ar & data.hash;
        }
    }
}
