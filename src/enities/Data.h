#pragma once

#include <vector>
#include <assert.h>
#include "../types/ByteVector.h"
#include "../types/HashValue.h"


class Data {
public:
    ByteVector data;
    HashValue hash;

    bool operator<(const Data &other) const {
        assert(hash.size() == other.hash.size());

        for (unsigned int i = 0; i < hash.size(); ++i) {
            if (hash[i] != other.hash[i]) {
                return hash[i] < other.hash[i];
            }
        }

        return false;
    }
};
